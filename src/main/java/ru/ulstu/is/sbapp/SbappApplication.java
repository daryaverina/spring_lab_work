package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.processing.Generated;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/")
	public String hello(@RequestParam(value="name", defaultValue = "world") String name){
		return String.format("Hello %s!", name);
	}

	@GetMapping("/task")
	public int del(@RequestParam(value="num1") int num1,
				   @RequestParam(value="num2") int num2){
		return num1/num2;
	}

}
